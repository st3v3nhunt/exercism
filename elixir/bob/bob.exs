defmodule Bob do
  def hey(input) do
    cond do
      nothing?(input) -> "Fine. Be that way!"
      question?(input) -> "Sure."
      shouting?(input) -> "Whoa, chill out!"
      true -> "Whatever."
    end
  end

  defp nothing?(input) do
    byte_size(String.strip(input)) == 0
  end

  defp question?(input) do
    String.ends_with?(input, "?")
  end

  defp shouting?(input) do
    String.match?(input, ~r/[\p{Lu}]/) and String.upcase(input) == input
  end
end
