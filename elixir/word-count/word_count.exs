defmodule Words do
  @doc """
  Count the number of words in the sentence.

  Words are compared case-insensitively.
  """
  @spec count(String.t) :: map()
  def count(sentence) do
    split_words = clean_and_split_input(sentence)
    count(%{}, split_words)
  end

  defp count(map, [h|t]) do
    if Map.has_key?(map, h) do
      Map.update!(map, h, &(&1+1))
      |> count(t)
    else
      Map.put(map, h, 1)
      |> count(t)
    end

  end
  defp count(map, []) do
    map
  end

  def clean_and_split_input(sentence) do
    sentence
    |> String.downcase
    |> String.split("")
    |> Enum.filter(fn (x) -> String.match? x, ~r/\w| |-/ end)
    |> Enum.join
    |> String.split([" ", "_"], trim: true)
  end
end
