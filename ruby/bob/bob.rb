class Bob

  def hey(word)
    if word.end_with?('?') && (word.upcase != word)
      'Sure.'
    elsif word.strip.empty?
      'Fine. Be that way!'
    else
      word.upcase == word ? 'Woah, chill out!' : 'Whatever.'
    end
  end

end
